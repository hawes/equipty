// page numero 1
import React from 'react';
import { StyleSheet,ImageBackground, Image, TextInput, Text, TouchableOpacity, View, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';


  const Login = ({ navigation }: { navigation: any }) => {
    return (
      
      <View style={styles.container} > 
     
       <View>
        <Image
          style={styles.imguser}
          source={require('./assets/icons/equipty.webp')}
        />
      </View>

        <View style={styles.input_container}>
          <TextInput placeholder={'E-mail'}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={email => /*onHandleChange("email", email)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}>

          </TextInput>
        </View>

        <View style={styles.input_container}>
          <TextInput placeholder={'Mot de passe'}
            secureTextEntry={true}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={mot_de_passe => onHandleChange("mot_de_passe", mot_de_passe)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}></TextInput>
        </View>

        <View style={styles.submit_btn_container}>
          <TouchableOpacity /*onPress={onSubmit}*/>
            <Text style={styles.submit_box}>
              Connexion
            </Text>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={{ color: '#000000', fontSize: 12, fontWeight: 'bold', width: '100%', right: -100 }}>Mot de passe oublié ?</Text>
        </View>


        <View style={{ flex: 5, flexDirection: 'row' }}>
          <Icon style={styles.icon}
            raised
            name='facebook'
            type='font-awesome'
            color='#DA050F'
            onPress={() => console.log('hello')} />


          <Icon style={styles.icon}
            raised
            name='instagram'
            type='font-awesome'
            color='#DA050F'
            onPress={() => console.log('hello')} />
          <Icon style={styles.icon}
            raised
            name='plus'
            type='font-awesome'
            color='#DA050F'
            onPress={() => navigation.navigate('InscriptionSupporteur_1', { screen: 'InscriptionSupporteur_1' })} />
        </View>


      </View>

    );
  }
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff'
  },
  title: {
    fontSize: 35,
    color: '#000000'
  },
  button: {
    padding: 10,
    marginVertical: 15,
    backgroundColor: '#000000'
  },
  buttonText: {
    color: '#000000'
  },
  img: {
    marginTop: '15%',
    marginBottom: '5%'

  },
  input_container: {
    width: '85%',
    height: 50,
    position: 'relative',
    margin: 5,
    paddingLeft: 4,
    paddingRight: 4,
    borderStyle: 'solid',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000000',
  },
  input_box: {
    backgroundColor: '#00000000',
    color: '#000000',
  },
  submit_btn_container: {
    backgroundColor: '#000000',
    height: 50,
    width: '45%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    //flex: 1, flexDirection: 'row' 
  },
  submit_box: { fontWeight: 'bold', color: '#ffffff' },
  stretch: {
    width: 70,
    height: 70,
  },
  icon: {
    margin: 30,
    fontSize: 45,
  },
  imguser: {   
    marginTop: '12%',    
    marginBottom: '3%',   
    backgroundColor: '#FFF',
    borderRadius: 200,
    height:100,
    width: 100
  },
  btnInscription: {
    backgroundColor: '#000000',
    borderColor: '#B22222',
    height: 30,
    width: '25%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  image: {
    flex: 1,
    justifyContent: "center"
  },


});

export default Login;