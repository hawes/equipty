/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


import Profile from './Profile';
import Login from './Login';
import InscriptionSupporteur_1 from './InscriptionSupporteur_1';
import InscriptionSupporteur_2 from './InscriptionSupporteur_2';
import InscriptionPro_1 from './InscriptionPro_1';


const Stack = createNativeStackNavigator();

const Section: React.FC<{
  title: string;
}> = ({children, title}) => {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <View style={styles.sectionContainer}>
      <Text
        style={[
          styles.sectionTitle,
          {
            color: isDarkMode ? Colors.white : Colors.black,
          },
        ]}>
        {title}
      </Text>
      <Text
        style={[
          styles.sectionDescription,
          {
            color: isDarkMode ? Colors.light : Colors.dark,
          },
        ]}>
        {children}
      </Text>
    </View>
  );
};

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    <NavigationContainer>
      <Stack.Navigator>

        <Stack.Screen
          name="Login"
          component={Login}
          options={{ title: 'Login'}}
        />
          <Stack.Screen
          name="Profile"
          component={Profile}
          options={{ title: 'profile' }}
        />
        <Stack.Screen
          name="InscriptionSupporteur_1"
          component={InscriptionSupporteur_1}
          options={{ title: 'Inscription_Supporteure_1' }}
        />
        <Stack.Screen
          name="InscriptionSupporteur_2"
          component={InscriptionSupporteur_2}
          options={{ title: 'Inscription_Supporteure_2' }}
        />
        <Stack.Screen
          name="InscriptionPro_1"
          component={InscriptionPro_1}
          options={{ title: 'InscriptionPro_1' }}
        />
      </Stack.Navigator>
    </NavigationContainer>

  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,

  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
