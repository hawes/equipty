// page numero 9
import React, { useState } from 'react';
import { StyleSheet, Picker, ScrollView, Image, TextInput, Text, TouchableOpacity, View, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';



const InscriptionPro_1 = ({ navigation }: { navigation: any }) => {
  const [isSelected, setToggleCheckBox] = useState(false)
  const [selectedValue, setSelectedValue] = useState("Ville");
  var pro = [
    { label: ' Profesionnel  ', value: 0 }
  ];
  var supporteur = [
    { label: ' Supporteur', value: 1 }
  ];


  return (
    <ScrollView contentContainerStyle={styles.contentContainer}>
      <View style={styles.container} > 
        

        <View>
          <Image
            style={styles.imguser}
            source={require('./assets/icons/equipty.webp')}
          />
        </View>

        <View style={styles.bloc}>
          <RadioForm
            radio_props={pro}
            initial={0}
            onPress={() => navigation.navigate('InscriptionPro_1', { screen: 'InscriptionPro_1' })}
          />
          <RadioForm
            radio_props={supporteur}
            initial={1}
            onPress={() => navigation.navigate('InscriptionSupporteur_1', { screen: 'InscriptionSupporteur_1' })}
          />

        </View>


        <View style={styles.input_container}>
          <TextInput placeholder={'Nom Enseigne'}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={email => /*onHandleChange("email", email)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}>
          </TextInput>
        </View>

        <View style={styles.input_container}>
          <TextInput placeholder={'Catégorie'}
            secureTextEntry={true}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={mot_de_passe => onHandleChange("mot_de_passe", mot_de_passe)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}></TextInput>
        </View>
        <View style={styles.input_container} >
          <Picker style={styles.select}
            selectedValue={selectedValue}
            onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
          >
            <Picker.Item label="Tunis" value="Tunis" />
            <Picker.Item label="Kélibia" value="Kélibia" />
            <Picker.Item label="Tunis" value="Tunis" />
            <Picker.Item label="Kélibia" value="Kélibia" />
            <Picker.Item label="Tunis" value="Tunis" />
            <Picker.Item label="Kélibia" value="Kélibia" />
            <Picker.Item label="Tunis" value="Tunis" />
          </Picker>
        </View>
        <View style={styles.input_container}>
          <TextInput placeholder={'Adresse'}
            secureTextEntry={true}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={mot_de_passe => onHandleChange("mot_de_passe", mot_de_passe)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}></TextInput>
        </View>  

        <View style={styles.input_container}>
          <TextInput placeholder={'Télephone'}
            keyboardType={"numeric"}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={email => /*onHandleChange("email", email)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}>
          </TextInput>
        </View>


        <View style={styles.input_container}>
          <TextInput placeholder={'URL page facebook'}
            maxLength={20}
            returnKeyType={"done"}
            secureTextEntry={true}
            /*onChangeText={email => /*onHandleChange("email", email)}*/
            style={styles.input_box} placeholderTextColor={'#000'}>
          </TextInput>
        </View>

        <View style={styles.input_container}>
          <TextInput placeholder={'URL page instagramme'}
            secureTextEntry={true}
            maxLength={20}
            returnKeyType={"done"}
            /*onChangeText={mot_de_passe => onHandleChange("mot_de_passe", mot_de_passe)}*/
            style={styles.input_box} placeholderTextColor={'#000000'}></TextInput>
        </View>




        <View style={styles.bloc}>
          <CheckBox
            tintColors={{ true: '#DA050F', false: '#DA050F' }}
            style={styles.checkbox}
            disabled={false}
            value={isSelected}
            onValueChange={(newValue) => setToggleCheckBox(newValue)}
          />
          <Text style={{ color: '#000', fontSize: 12, margin: 7, fontWeight: 'bold', width: '100%', right: -20 }}>Veuillez confirmer la charte de l'application en cliquant ici</Text>
        </View>

        <View style={styles.submit_btn_container}>
          <TouchableOpacity
            onPress={() => navigation.navigate('InscriptionSupporteur_2', { screen: 'InscriptionSupporteur_2' })}>
            <Text style={styles.submit_box}>
              Suivant
            </Text>
          </TouchableOpacity>
        </View>




      </View>
    </ScrollView>

  );
 
      
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  contentContainer: {
    paddingVertical: 0, 
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff'
  },
  title: {
    fontSize: 35,
    color: '#fff'
  },
  button: {
    padding: 10,
    marginVertical: 15,
    backgroundColor: '#0645AD'
  },
  buttonText: {
    color: '#fff'
  },
  img: {
    marginTop: '15%',
    marginBottom: '5%'

  },
  input_container: {
    width: '85%',
    height: 50,
    position: 'relative',
    margin: 5,
    paddingLeft: 4,
    paddingRight: 4,
    borderStyle: 'solid',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000000',
    color: '#000',
  },
  input_box: {
    backgroundColor: '#00000000',
    color: '#000',
  },
  submit_btn_container: {
    backgroundColor: '#DA050F',
    height: 50,
    width: '45%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    //flex: 1, flexDirection: 'row' 
  },
  submit_box: { fontWeight: 'bold', color: '#ffffff' },
  stretch: {
    width: 70,
    height: 70,
  },
  icon: {
    margin: 30,
    fontSize: 45,
  },
  imguser: {
    //marginBottom: '3%',
    backgroundColor: '#FFF',
    borderRadius: 200,
    height: 100,
    width: 100
  },
  btnInscription: {
    backgroundColor: '#DCDCDC',
    borderColor: '#B22222',
    height: 30,
    width: '25%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },

  bloc: {
    flexDirection: "row",
    //marginBottom: 20,

  },
  checkbox: {
    alignSelf: "center",
    right: -25,


  },
  select: {
    //marginTop: -2,
    marginLeft: -3,
    color: '#000000'

  }

});

export default InscriptionPro_1;