// page numero 2
import React, { useState } from 'react';
import { StyleSheet, Image, Picker,ScrollView, TextInput, Text, TouchableOpacity, View, TouchableHighlight } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import CheckBox from '@react-native-community/checkbox';



const InscriptionSupporteur_2 = ({ navigation }: { navigation: any }) => {
  const [isSelected, setToggleCheckBox] = useState(false)
  const [selectedValue, setSelectedValue] = useState(" ");
  return (
    <ScrollView contentContainerStyle={styles.contentContainer}>
    <View style={styles.container} >    
    <View>
      <Image
        style={styles.imguser}
        source={require('./assets/icons/equipty.webp')}
      />
    </View> 

      <View>
        <Text style={{ color: '#000000', fontSize: 12, marginTop: 10, fontWeight: 'bold', width: '65%', right: 10 }}>
          Si vous n'etes pas interessé, veuillez ignorer cette étape</Text>
        <Icon style={styles.icon}
          raised
          name='forward'
          type='font-awesome'
          color='#000000'
          onPress={() => console.log('hello')} />
      </View>


      <View style={{ width: '100%', margin: 10, }}>
        <Text style={{ color: '#000000', fontSize: 15, fontWeight: 'bold', right: -4 }}>
          Choisir votre équipe :</Text>
      </View>
      <View style={styles.input_container} >
        <Picker style={styles.select}
          selectedValue={selectedValue}
          onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        >
          <Picker.Item label="Tunis" value="Tunis" />
          <Picker.Item label="Kélibia" value="Kélibia" />
          <Picker.Item label="Tunis" value="Tunis" />
          <Picker.Item label="Kélibia" value="Kélibia" />
          <Picker.Item label="Tunis" value="Tunis" />
          <Picker.Item label="Kélibia" value="Kélibia" />
          <Picker.Item label="Tunis" value="Tunis" />
        </Picker>
      </View>


      <View style={{ width: '100%', margin: 10, }} >
        <Text style={{ color: '#000000', fontSize: 15, fontWeight: 'bold', left: 5 }}>
          Numéro carte d'abonnement :</Text>
      </View>


      <View style={styles.input_container} >
        <TextInput placeholder={'numéro abonnement'}
          secureTextEntry={true}
          maxLength={20}
          returnKeyType={"done"}
          /*onChangeText={mot_de_passe => onHandleChange("mot_de_passe", mot_de_passe)}*/
          style={styles.input_box} placeholderTextColor={'#000000'}></TextInput>
      </View>


      <View style={{ flexDirection: 'row' }}>
        <View style={styles.submit_btn_container}>
          <TouchableOpacity  onPress={() => navigation.navigate('InscriptionSupporteur_1', { screen: 'InscriptionSupporteur_1' })}>
            <Text style={styles.submit_box}>
              <Icon style={styles.icon_2}
                raised
                name='backward'
                type='font-awesome'
                color='#fff'
                onPress={() => console.log('hello')} />
              _Précedent
            </Text>
          </TouchableOpacity>

        </View>
        <View style={styles.submit_btn_container}>
          <TouchableOpacity /*onPress={onSubmit}*/>
            <Text style={styles.submit_box}>
              Suivant_
              <Icon style={styles.icon_2}
                raised
                name='forward'
                type='font-awesome'
                color='#fff'
                onPress={() => console.log('hello')} /> </Text>
          </TouchableOpacity>

        </View>

      </View>

      </View>
    </ScrollView>


  );
}

const styles = StyleSheet.create({
  contentContainer: {
    //paddingVertical: 100, 
   backgroundColor:'#fff',
   height: '100%'
  },
  container: {
   // flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',    
  },
  title: {
    fontSize: 35,
    color: '#fff'
  },
  button: {
    padding: 10,
    marginVertical: 15,
    backgroundColor: '#0645AD'
  },
  buttonText: {
    color: '#fff'
  },
  img: {
    marginTop: '15%',
    marginBottom: '5%'

  },
  input_container: {
    width: '85%',
    height: 50,
    position: 'relative',
    margin: 5,
    paddingLeft: 4,
    paddingRight: 4,
    borderStyle: 'solid',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#000000',
  },
  input_container_2: {
    width: '96%',
    height: 40,
    position: 'relative',
    //margin: 5,
    paddingLeft: 4,
    paddingRight: 4,
    borderStyle: 'solid',
    borderRadius: 30,
    borderWidth: 1,
    borderColor: '#FF6347',
  },
  input_box: {
    backgroundColor: '#00000000',
    color: '#FFF',
  },
  submit_btn_container: {
    backgroundColor: '#DA050F',
    height: 50,
    width: '40%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 10,
    justifyContent: 'center',
    //flex: 1, flexDirection: 'row' 
  },
  submit_box: { fontWeight: 'bold', color: '#ffffff' },
  stretch: {
    width: 70,
    height: 70,
  },
  icon: {
    marginTop: 0,
    fontSize: 25,
  },
  icon_2: {

    fontSize: 15,

  },
  imguser: {
    //marginBottom: '3%',
    backgroundColor: '#FFF',
    borderRadius: 200,
    height: 100,
    width: 100
  },
  btnInscription: {
    backgroundColor: '#DCDCDC',
    borderColor: '#B22222',
    height: 30,
    width: '25%',
    margin: 15,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },

  checkboxContainer: {
    flexDirection: "row",
    //marginBottom: 20,

  },
  checkbox: {
    alignSelf: "center",
    right: -25,

  },
  select: {
    marginTop: -7,
    marginLeft: -2,
    color: '#000000'

  }


});

export default InscriptionSupporteur_2;